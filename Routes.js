// import React from 'react'
// import { Router, Scene } from 'react-native-router-flux'
// import MovieDB from './src/index'

// const Routes = () => (
//   <Router>
//     <Scene key="root">
//       <Scene
//         key="Movies"
//         component={MovieDB}
//         title="Movie dictionnary"
//         initial={true}
//       />
//     </Scene>
//   </Router>
// )
// export default Routes

import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import MovieDB from './src/index'

const Stack = createStackNavigator()

export default function Root() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Movies Dictionnary" component={MovieDB} />
    </Stack.Navigator>
  )
}
