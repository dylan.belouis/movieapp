// import { StatusBar } from 'expo-status-bar'
// import React from 'react'
// import { StyleSheet, Text, View } from 'react-native'
// import Routes from './Routes'

// import { useFonts, Inter_900Black } from '@expo-google-fonts/inter'

// const App = () => {
//   let [fontsLoaded] = useFonts({
//     Inter_900Black,
//   })
//   return Routes
// }

import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'

import Root from './Routes'

export default function App() {
  return (
    <NavigationContainer>
      <Root />
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Inter_900Black',
    fontSize: 40,
  },
})
