import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TextInput, Image, Button } from 'react-native'
import axios from 'axios'
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter'

const MovieDB = () => {
  const [data, setData] = useState([])
  const [filter, setFilter] = useState('')
  const [textInputValue, setTextInputValue] = useState('')
  const [idFilter, setFilterId] = useState('')
  const [movieTitle, setMovieTitle] = useState('')

  let [fontsLoaded] = useFonts({
    Inter_900Black,
  })

  const optionsId = {
    method: 'GET',
    url: 'https://movie-database-imdb-alternative.p.rapidapi.com/',
    params: { plot: 'full', r: 'json', i: 'tt4154796' },
    headers: {
      'x-rapidapi-host': 'movie-database-imdb-alternative.p.rapidapi.com',
      'x-rapidapi-key': '233e58f7e2msh0f11bfe2a829d3ep18a1e3jsn2dd3aaa4f626',
    },
  }

  const options = {
    method: 'GET',
    url: `https://movie-database-imdb-alternative.p.rapidapi.com`,
    params: { title: `${textInputValue}`, r: 'json', plot: 'full', page: '1' },
    headers: {
      'x-rapidapi-host': 'movie-database-imdb-alternative.p.rapidapi.com',
      'x-rapidapi-key': '233e58f7e2msh0f11bfe2a829d3ep18a1e3jsn2dd3aaa4f626',
    },
  }

  const optionsFilter = {
    method: 'GET',
    url: 'https://movie-database-imdb-alternative.p.rapidapi.com/',
    params: {
      s: `${textInputValue}`,
      r: 'json',
      plot: 'full',
      type: `${filter}`,
      page: '1',
    },
    headers: {
      'x-rapidapi-host': 'movie-database-imdb-alternative.p.rapidapi.com',
      'x-rapidapi-key': '233e58f7e2msh0f11bfe2a829d3ep18a1e3jsn2dd3aaa4f626',
    },
  }

  const getId = (id) => {
    setFilterId(id)
    setFilter('id')
    console.log(filter)
  }

  useEffect(() => {
    if (filter !== 'none') {
      axios
        .request(optionsFilter)
        .then(function (response) {
          console.log(response.data.Search)
          setData(response.data.Search)
        })
        .catch(function (error) {
          console.error(error)
        })
    } else if (filter === 'id') {
      axios
        .request(optionsId)
        .then(function (response) {
          console.log([response])
          //   setData(response.data)
        })
        .catch(function (error) {
          console.error(error)
        })
    } else if (filter === 'none') {
      axios
        .request(options)
        .then(function (response) {
          console.log(response.data.Search)
          setData(response.data.Search)
        })
        .catch(function (error) {
          console.error(error)
        })
    }
  }, [textInputValue, idFilter])

  return (
    <View style={styles.ViewDisplay}>
      <Text style={styles.titleMovie}>FreeMovie</Text>
      <TextInput
        style={styles.input}
        onChangeText={setTextInputValue}
        value={textInputValue}
      />
      <View style={styles.divGlobalFilter}>
        <View title="Films" style={styles.divItemFilter}>
          <Button onPress={() => setFilter('movie')} title="Films" />
        </View>
        <View style={styles.divItemFilter}>
          <Button onPress={() => setFilter('series')} title="Series" />
        </View>
        <View style={styles.divItemFilter}>
          <Button onPress={() => setFilter('episode')} title="Episodes" />
        </View>
        <View style={styles.divItemFilter}>
          <Button onPress={() => setFilter('none')} title="Tout" />
        </View>
      </View>
      {data &&
        data.map((d, index) => (
          <View style={styles.ViewCard} key={`${d.id}-${index}`}>
            <Text style={styles.titleText}>{d.Title}</Text>
            <Text style={styles.titleText}>{d.Country}</Text>
            <Image style={styles.tinyLogo} source={d.Poster} />
            <Text style={styles.yearText}>{d.Year}</Text>
            <Button onPress={() => getId(d.imdbID)} title={d.imdbID} />
          </View>
        ))}
    </View>
  )
}

const styles = StyleSheet.create({
  ViewDisplay: {
    width: '100vw',
    minHeight: '100vh',
    color: 'white',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#424242',
  },
  titleMovie: {
    paddingVertical: 15,
    paddingLeft: 15,
    width: '100%',
    color: 'white',
    fontSize: 20,
  },
  titleText: {
    paddingBottom: 20,
    fontFamily: 'Inter_900Black',
    fontSize: 20,
  },
  divGlobalFilter: {
    paddingTop: 10,
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  divItemFilter: {},
  itemFilter: {
    color: 'white',
  },
  input: {
    height: 50,
    fontSize: 20,
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    marginTop: 50,
  },
  ViewCard: {
    width: '80%',
    margin: 'auto',
    marginTop: '60px',
    textAlign: 'center',
    backgroundColor: 'white',
    color: 'black',
    padding: 10,
  },
  tinyLogo: {
    width: '100%',
    height: '500px',
  },
  yearText: {
    marginTop: 10,
    fontFamily: 'Inter_900Black',
    fontSize: 20,
  },
})

export default MovieDB
